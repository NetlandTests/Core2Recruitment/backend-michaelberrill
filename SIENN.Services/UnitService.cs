﻿using SIENN.DbAccess;
using SIENN.DbAccess.Model;
using System.Threading.Tasks;

namespace SIENN.Services
{
    public class UnitService : IUnitService
    {
        private readonly ApplicationContext context;

        public UnitService(ApplicationContext context)
        {
            this.context = context;
        }

        public async Task Add(Unit unit)
        {
            context.Units.Add(unit);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Unit unit)
        {
            context.Units.Remove(unit);
            await context.SaveChangesAsync();
        }

        public async Task<Unit> GetAsync(string code)
        {
            return await context.Units.FindAsync(code);
        }

        public async Task Update(Unit unit)
        {
            // get the Unit
            // update the description only
            var existingType = await context.Units.FindAsync(unit.Code);
            existingType.Description = unit.Description;
            await context.SaveChangesAsync();
        }
    }
}
