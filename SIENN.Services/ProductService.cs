﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess;
using SIENN.DbAccess.Model;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;

namespace SIENN.Services
{
    public class ProductService : IProductService
    {
        private readonly ApplicationContext context;
        private readonly ILogger<ProductService> logger;

        public ProductService(ApplicationContext context, ILogger<ProductService> logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public async Task AddProduct(Product product)
        {
            context.Products.Add(product);
            await context.SaveChangesAsync();
        }

        public async Task<Product> GetProductByCodeAsync(string code)
        {
            return await context.Products.Include("ProductCategories")
                                         .Include("Type")
                                         .Include("Unit").FirstOrDefaultAsync(x => x.Code == code);
        }

        public async Task DeleteAsync(Product product)
        {
            context.Products.Remove(product);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Product product)
        {
            var existingProduct = await GetProductByCodeAsync(product.Code);
            existingProduct.Description = product.Description;
            existingProduct.DeliveryDateUtc = product.DeliveryDateUtc;
            existingProduct.IsAvailable = product.IsAvailable;
            existingProduct.Price = product.Price;
            existingProduct.TypeCode = product.TypeCode;
            existingProduct.UnitCode = product.UnitCode;
            existingProduct.ProductCategories.Clear();
            foreach(var productCategory in product.ProductCategories)
            {
                existingProduct.ProductCategories.Add(productCategory);
            }
            await context.SaveChangesAsync();
        }

        public async Task<PageResult<Product>> GetAvailableProductsAsync(int pageNo, int pageSize)
        {
            Expression<Func<Product, bool>> productQuery = x => x.IsAvailable == true;

            return await QueryProducts(pageNo, pageSize, productQuery);
        }

        private async Task<PageResult<Product>> QueryProducts(int pageNo, int pageSize, Expression<Func<Product, bool>> productQuery)
        {
            var totalCount = context.Products.Count(productQuery);

            var products = await context.Products.Include("ProductCategories")
                                                  .Include("Type")
                                                  .Include("Unit")
                                                  .Where(productQuery).Skip(pageSize * pageNo).Take(pageSize).ToArrayAsync();

            return new PageResult<Product>()
            {
                Page = pageNo,
                PageSize = pageSize,
                Items = products,
                ItemsCount = totalCount
            };
        }

        public async Task<PageResult<Product>> GetProductsFilteredAsync(int pageNo, int pageSize, string typeCode, string unitCode, string categoryCode)
        {
            logger.LogDebug("Creating product filter query {pageNo}, {pageSize}, {typeCode}, {unitCode}, {categoryCode}", pageNo, pageSize, typeCode, unitCode, categoryCode);
            Expression<Func<Product, bool>> productQuery = x =>
            (typeCode == null || x.TypeCode.ToUpper() == typeCode.ToUpper()) &&
            (unitCode == null || x.UnitCode.ToUpper() == unitCode.ToUpper())&&
            (categoryCode == null || x.ProductCategories.Any(c => c.CategoryCode.ToUpper() == categoryCode.ToUpper()));

            return await QueryProducts(pageNo, pageSize, productQuery);
        }
    }
}
