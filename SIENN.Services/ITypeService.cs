﻿using System.Threading.Tasks;
using SIENN.DbAccess.Model;

namespace SIENN.Services
{
    public interface ITypeService
    {
        Task Add(Type type);
        Task DeleteAsync(Type type);
        Task<Type> GetAsync(string code);
        Task Update(Type type);
    }
}