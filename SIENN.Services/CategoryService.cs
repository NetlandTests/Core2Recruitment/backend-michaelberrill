﻿using SIENN.DbAccess;
using SIENN.DbAccess.Model;
using System.Threading.Tasks;

namespace SIENN.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationContext context;

        public CategoryService(ApplicationContext context)
        {
            this.context = context;
        }

        public async Task AddCategory(Category category)
        {
            context.Categories.Add(category);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Category category)
        {
            context.Categories.Remove(category);
            await context.SaveChangesAsync();
        }

        public async Task<Category> GetCategoryByCodeAsync(string code)
        {
            return await context.Categories.FindAsync(code);
        }

        public async Task UpdateCategory(Category category)
        {
            // get the category
            // update the description only
            var existingCategory = await context.Categories.FindAsync(category.Code);
            existingCategory.Description = category.Description;
            await context.SaveChangesAsync();
        }
    }
}
