﻿using SIENN.DbAccess;
using SIENN.DbAccess.Model;
using System.Threading.Tasks;

namespace SIENN.Services
{
    public class TypeService : ITypeService
    {
        private readonly ApplicationContext context;

        public TypeService(ApplicationContext context)
        {
            this.context = context;
        }

        public async Task Add(Type type)
        {
            context.Types.Add(type);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Type type)
        {
            context.Types.Remove(type);
            await context.SaveChangesAsync();
        }

        public async Task<Type> GetAsync(string code)
        {
            return await context.Types.FindAsync(code);
        }

        public async Task Update(Type type)
        {
            // get the type
            // update the description only
            var existingType = await context.Types.FindAsync(type.Code);
            existingType.Description = type.Description;
            await context.SaveChangesAsync();
        }
    }
}
