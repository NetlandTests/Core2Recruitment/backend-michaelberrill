﻿using System.Threading.Tasks;
using SIENN.DbAccess.Model;

namespace SIENN.Services
{
    public interface ICategoryService
    {
        Task AddCategory(Category category);
        Task<Category> GetCategoryByCodeAsync(string code);
        Task UpdateCategory(Category category);
        Task DeleteAsync(Category category);
    }
}