﻿using System.Threading.Tasks;
using SIENN.DbAccess.Model;

namespace SIENN.Services
{
    public interface IUnitService
    {
        Task Add(Unit unit);
        Task DeleteAsync(Unit unit);
        Task<Unit> GetAsync(string code);
        Task Update(Unit unit);
    }
}