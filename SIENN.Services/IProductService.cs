﻿using SIENN.DbAccess.Model;
using System.Threading.Tasks;

namespace SIENN.Services
{
    public interface IProductService
    {
        Task<Product> GetProductByCodeAsync(string code);
        Task AddProduct(Product product);
        Task UpdateAsync(Product product);
        Task DeleteAsync(Product product);
        Task<PageResult<Product>> GetAvailableProductsAsync(int pageNo, int pageSize);
        Task<PageResult<Product>> GetProductsFilteredAsync(int pageNo, int pageSize, string typeCode, string unitCode, string categoryCode);
    }
}