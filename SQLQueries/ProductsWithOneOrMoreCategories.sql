-- get all products where they have one ore more Product categories
SELECT DISTINCT("Code")
FROM public."Products" as p
INNER JOIN public."ProductCategory" pc on (pc."ProductCode" = p."Code")
WHERE "IsAvailable" = TRUE