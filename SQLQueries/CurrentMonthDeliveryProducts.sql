-- Get all Products with Delivery date in the current month
SELECT "Code", "DeliveryDateUtc"
FROM public."Products"
WHERE "IsAvailable" = FALSE 
AND EXTRACT(MONTH FROM "DeliveryDateUtc") = EXTRACT(MONTH FROM CURRENT_TIMESTAMP);