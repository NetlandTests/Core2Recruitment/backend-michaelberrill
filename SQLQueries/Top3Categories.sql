-- top 3 categories

select "CategoryCode" from 
(select count("CategoryCode") as "Occurrences","CategoryCode" 
 from public."ProductCategory" 
 group by "CategoryCode" 
 order by "Occurrences" DESC) AS TEMP limit 3
