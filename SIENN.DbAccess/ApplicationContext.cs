﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Model;
using System.Linq;

namespace SIENN.DbAccess
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(x => x.Code);
            modelBuilder.Entity<Product>().Property(x => x.Code).ValueGeneratedNever();
            modelBuilder.Entity<Product>().Property(x => x.Code).HasMaxLength(10);
            modelBuilder.Entity<Product>().Property(x => x.Description).HasMaxLength(500);

            modelBuilder.Entity<Unit>().HasKey(x => x.Code);
            modelBuilder.Entity<Unit>().Property(x => x.Code).ValueGeneratedNever();
            
            modelBuilder.Entity<Type>().HasKey(x => x.Code);
            modelBuilder.Entity<Type>().Property(x => x.Code).ValueGeneratedNever();
            
            modelBuilder.Entity<Category>().HasKey(x => x.Code);
            modelBuilder.Entity<Category>().Property(x => x.Code).ValueGeneratedNever();

            modelBuilder.Entity<ProductCategory>().HasKey(x => new { x.ProductCode, x.CategoryCode });
            modelBuilder.Entity<ProductCategory>().HasOne(x => x.Product).WithMany(p => p.ProductCategories).HasForeignKey(c => c.ProductCode).OnDelete(DeleteBehavior.Cascade); // if we delete the product then delete the product categories too
            modelBuilder.Entity<ProductCategory>().HasOne(x => x.Category).WithMany(p => p.ProductCategories).HasForeignKey(c => c.CategoryCode).OnDelete(DeleteBehavior.Restrict); // do not let them delete a category if there is a product category present that references it

            base.OnModelCreating(modelBuilder);
        }
    }
}
