﻿namespace SIENN.DbAccess.Model
{
    public class ProductCategory
    {
        public string ProductCode { get; set; }
        public Product Product { get; set; }

        public string CategoryCode { get; set; }
        public Category Category { get; set; }
    }
}
