﻿namespace SIENN.DbAccess.Model
{
    public class PageResult<T>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int ItemsCount { get; set; }
        public int TotalPages { get { return (ItemsCount > 0 && PageSize > 0) ? (ItemsCount + PageSize - 1) / PageSize : 0; } }
        public T[] Items { get; set; }
    }
}
