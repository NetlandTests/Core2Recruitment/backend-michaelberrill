﻿using System;
using System.Collections.Generic;

namespace SIENN.DbAccess.Model
{
    public class Product
    {        
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime DeliveryDateUtc { get; set; }

        public Type Type { get; set; }
        public string TypeCode { get; set; }
        public Unit Unit { get; set; }
        public string UnitCode { get; set; }

        public ICollection<ProductCategory> ProductCategories { get; set; }
    }
}