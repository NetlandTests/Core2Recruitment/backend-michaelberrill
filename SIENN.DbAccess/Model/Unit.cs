﻿using System.Collections.Generic;

namespace SIENN.DbAccess.Model
{
    public class Unit
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
