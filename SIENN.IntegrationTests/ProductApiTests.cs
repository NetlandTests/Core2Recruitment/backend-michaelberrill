﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using NUnit.Framework;
using SIENN.DbAccess;
using SIENN.WebApi;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using SIENN.DbAccess.Model;
using System.Net;
using Newtonsoft.Json;
using SIENN.WebApi.Models;

namespace SIENN.IntegrationTests
{
    [TestFixture]
    public class ProductApiTests
    {
        private TestServer _server;
        private HttpClient _client;
        private ApplicationContext _context;

        [SetUp]
        public void SetupBeforeEachTest()
        {
            var builder = new WebHostBuilder()
            .UseEnvironment("Testing")
            .UseStartup<Startup>();

            _server = new TestServer(builder);

            _context = _server.Host.Services.GetService(typeof(ApplicationContext)) as ApplicationContext;

            _client = _server.CreateClient();
        }

        [TearDown]
        public void TearDownAfterEachTest()
        {
            _server.Dispose();
            _client.Dispose();
        }

        [Test]
        public async Task CreateProduct_WithallPropertiesSet_ReturnsSuccessStatusCode()
        {
            // arrange

            // add a category
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat1', 'description' : 'Category 1'}", Encoding.UTF8, "application/json"));

            // add a type
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type1', 'description' : 'Type 1'}", Encoding.UTF8, "application/json"));

            // add a unit
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'Kg', 'description' : 'Kilograms'}", Encoding.UTF8, "application/json"));

            //act
            var response = await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod1', 'description' : 'Product 1', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type1', 'Unitcode': 'Kg'}", Encoding.UTF8, "application/json"));

            //assert
            Assert.IsTrue(response.IsSuccessStatusCode);

            var product = _context.Products.Include("ProductCategories").FirstOrDefault(x => x.Code == "Prod1");
            Assert.AreEqual("cat1", product.ProductCategories.ToArray()[0].CategoryCode);
            Assert.AreEqual("type1", product.TypeCode);
            Assert.AreEqual("Kg", product.UnitCode);
        }

        [Test]
        public async Task CreateProduct_WithValidProduct_ReturnsSuccessStatusCode()
        {
            // arrange
            var content = new StringContent("{'code': 'Prod1', 'description' : 'Product 1'}", Encoding.UTF8, "application/json");

            // act
            var response = await _client.PostAsync("/api/Product", content);

            // assert

            Assert.IsTrue(response.IsSuccessStatusCode);

            var product = _context.Products.Find("Prod1");
            Assert.IsNotNull(product);
        }

        [Test]
        public async Task CreateProduct_WithCategories_ReturnsSuccessStatusCode()
        {
            // arrange

            _context.Categories.Add(new Category { Code = "cat1", Description = "Category 1" });
            await _context.SaveChangesAsync();


            var content = new StringContent("{'code': 'Prod1', 'description' : 'Product 1', 'productCategoryCodes':['cat1']}", Encoding.UTF8, "application/json");

            // act
            var response = await _client.PostAsync("/api/Product", content);

            //assert
            Assert.IsTrue(response.IsSuccessStatusCode);

            var product = _context.Products.Include("ProductCategories").FirstOrDefault(x => x.Code == "Prod1");
            Assert.AreEqual("cat1", product.ProductCategories.ToArray()[0].CategoryCode);
        }



        [Test]
        public async Task Get_WhenNoProducts_ReturnsNotFound()
        {
            var response = await _client.GetAsync("/api/Product/abcd");

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async Task Get_WhenProductExists_ReturnsProduct()
        {

            _context.Categories.Add(new Category { Code = "cat1", Description = "Category 1" });

            _context.Products.Add(new Product
            {
                Code = "abcd",
                Description = "My product",
                ProductCategories = new[] { new ProductCategory { ProductCode = "abcd", CategoryCode = "cat1" } }
            });

            await _context.SaveChangesAsync();

            //act
            var response = await _client.GetAsync("/api/Product/abcd");

            Assert.IsTrue(response.IsSuccessStatusCode);

            var productJson = await response.Content.ReadAsStringAsync();

            var product = JsonConvert.DeserializeObject<ProductDto>(productJson);

            Assert.IsNotNull(product);
        }
        [Test]
        public async Task UpdateProduct_WithallPropertiesSet_ReturnsSuccessStatusCode()
        {
            // arrange

            // add a category
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat1', 'description' : 'Category 1'}", Encoding.UTF8, "application/json"));
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat2', 'description' : 'Category 2'}", Encoding.UTF8, "application/json"));

            // add a type
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type1', 'description' : 'Type 1'}", Encoding.UTF8, "application/json"));
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type2', 'description' : 'Type 2'}", Encoding.UTF8, "application/json"));

            // add a unit
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'Kg', 'description' : 'Kilograms'}", Encoding.UTF8, "application/json"));
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'm3', 'description' : 'cubic Meters'}", Encoding.UTF8, "application/json"));

            //act
            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod1', 'description' : 'Product 1', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type1', 'Unitcode': 'Kg'}", Encoding.UTF8, "application/json"));

            var response = await _client.PutAsync("/api/Product", new StringContent(
                "{'code': 'Prod1', 'description' : 'An Amazing Product', 'productCategoryCodes':['cat2'], 'TypeCode' : 'type2', 'Unitcode': 'm3'}", Encoding.UTF8, "application/json"));

            //assert
            Assert.IsTrue(response.IsSuccessStatusCode);

            var product = _context.Products.Include("ProductCategories").FirstOrDefault(x => x.Code == "Prod1");
            Assert.AreEqual("cat2", product.ProductCategories.ToArray()[0].CategoryCode);
            Assert.AreEqual("type2", product.TypeCode);
            Assert.AreEqual("m3", product.UnitCode);
        }

        [Test]
        public async Task GetPagedAvailable_ReturnsOnlyAvailableProducts()
        {
            // arrange

            // add a category
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat1', 'description' : 'Category 1'}", Encoding.UTF8, "application/json"));

            // add a type
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type1', 'description' : 'Type 1'}", Encoding.UTF8, "application/json"));

            // add a unit
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'Kg', 'description' : 'Kilograms'}", Encoding.UTF8, "application/json"));

            //act
            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod1', 'description' : 'Product 1', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type1', 'Unitcode': 'Kg', 'IsAvailable':'true'}", Encoding.UTF8, "application/json"));
                        
            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod2', 'description' : 'Product 2', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type1', 'Unitcode': 'Kg', 'IsAvailable':'false'}", Encoding.UTF8, "application/json"));

            //assert
            var response = await _client.GetAsync("/api/Product/Available?pageNo=0&pageSize=10");

            Assert.IsTrue(response.IsSuccessStatusCode);

            var productJson = await response.Content.ReadAsStringAsync();

            var productsPaged = JsonConvert.DeserializeObject< PageResult<ProductDto >>(productJson);

            Assert.AreEqual(1, productsPaged.ItemsCount);
            Assert.AreEqual("Prod1", productsPaged.Items[0].Code);
        }

        [Test]
        public async Task FilterProducts_ReturnsFilteredProductsOnly()
        {
            // arrange

            // add a category
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat1', 'description' : 'Category 1'}", Encoding.UTF8, "application/json"));
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat2', 'description' : 'Category 2'}", Encoding.UTF8, "application/json"));

            // add a type
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type1', 'description' : 'Type 1'}", Encoding.UTF8, "application/json"));
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type2', 'description' : 'Type 2'}", Encoding.UTF8, "application/json"));

            // add a unit
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'Kg', 'description' : 'Kilograms'}", Encoding.UTF8, "application/json"));
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'm3', 'description' : 'cubic meters'}", Encoding.UTF8, "application/json"));

            //act
            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod1', 'description' : 'Product 1', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type1', 'Unitcode': 'Kg', 'IsAvailable':'true'}", Encoding.UTF8, "application/json"));

            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod2', 'description' : 'Product 2', 'productCategoryCodes':['cat2'], 'TypeCode' : 'type1', 'Unitcode': 'm3', 'IsAvailable':'false'}", Encoding.UTF8, "application/json"));

            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod3', 'description' : 'Product 2', 'productCategoryCodes':['cat1', 'cat2'], 'TypeCode' : 'type1', 'Unitcode': 'Kg', 'IsAvailable':'false'}", Encoding.UTF8, "application/json"));

            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod4', 'description' : 'Product 2', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type2', 'Unitcode': 'Kg', 'IsAvailable':'false'}", Encoding.UTF8, "application/json"));

            //act
            var response = await _client.GetAsync("/api/Product/Filter?pageNo=0&pageSize=10&TypeCode=type1&Unitcode=Kg&categoryCode=cat1");

            //assert

            Assert.IsTrue(response.IsSuccessStatusCode);

            var productJson = await response.Content.ReadAsStringAsync();

            var productsPaged = JsonConvert.DeserializeObject<PageResult<ProductDto>>(productJson);

            Assert.AreEqual(2, productsPaged.ItemsCount);
            Assert.AreEqual("Prod1", productsPaged.Items[0].Code);
            Assert.AreEqual("Prod3", productsPaged.Items[1].Code);
        }


        [Test]
        public async Task GetProductSummary_ReturnsProductDtoSummary()
        {
            // arrange

            // add a category
            await _client.PostAsync("/api/Category", new StringContent("{'code': 'cat1', 'description' : 'Category 1'}", Encoding.UTF8, "application/json"));

            // add a type
            await _client.PostAsync("/api/Type", new StringContent("{'code': 'type1', 'description' : 'Type 1'}", Encoding.UTF8, "application/json"));

            // add a unit
            await _client.PostAsync("/api/Unit", new StringContent("{'code': 'Kg', 'description' : 'Kilograms'}", Encoding.UTF8, "application/json"));

            await _client.PostAsync("/api/Product", new StringContent(
                "{'code': 'Prod1', 'description' : 'Product 1', 'productCategoryCodes':['cat1'], 'TypeCode' : 'type1', 'Unitcode': 'Kg', 'DeliveryDateUtc':'2012-12-12T00:00:00', 'IsAvailable':'false', 'Price':'12.99'}", Encoding.UTF8, "application/json"));

            //act

            var response = await _client.GetAsync("/api/Product/summary/Prod1");

            //assert
            var productJson = await response.Content.ReadAsStringAsync();

            var productSummary = JsonConvert.DeserializeObject<ProductSummaryDto>(productJson);
            Assert.AreEqual("(Prod1) Product 1", productSummary.ProductDescription);
            Assert.AreEqual(1, productSummary.CategoriesCount);
            Assert.AreEqual("12.12.2012", productSummary.DeliveryDate);
            Assert.AreEqual("Niedostępny", productSummary.IsAvailable);
            Assert.AreEqual("12.99 zł", productSummary.Price);
            Assert.AreEqual("(type1) Type 1", productSummary.Type);
            Assert.AreEqual("(Kg) Kilograms", productSummary.Unit);
        }

        [Test]
        public async Task Post_WithNoCode_ReturnsBadRequest()
        {
            var content = new StringContent("{'description' : 'Product 1'}", Encoding.UTF8, "application/json");

            // act
            var response = await _client.PostAsync("/api/Product", content);

            // assert
            
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}