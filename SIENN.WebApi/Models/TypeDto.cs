﻿namespace SIENN.WebApi.Models
{
    public class TypeDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
