﻿namespace SIENN.WebApi.Models
{
    public class CategoryDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
