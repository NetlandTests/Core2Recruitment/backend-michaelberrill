﻿namespace SIENN.WebApi.Models
{
    public class UnitDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
