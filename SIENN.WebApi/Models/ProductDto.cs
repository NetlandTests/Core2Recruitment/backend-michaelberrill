﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIENN.WebApi.Models
{
    public class ProductDto
    {
        [MaxLength(10, ErrorMessage ="Code must be 10 characters or less")]
        [MinLength(2, ErrorMessage ="Code must be more than 2 characters")]
        [Required]
        public string Code { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
        public decimal Price { get; set; }        
        public bool IsAvailable { get; set; } = true; // default to true if not set        
        public DateTime DeliveryDateUtc { get; set; }
        public string TypeCode { get; set; }
        public string UnitCode { get; set; }
        public ICollection<string> ProductCategoryCodes { get; set; }
    }
}
