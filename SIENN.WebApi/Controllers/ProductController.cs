﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.WebApi.Models;
using System.Threading.Tasks;
using SIENN.WebApi.Mappers;
using System;
using Microsoft.Extensions.Logging;
using System.Net;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Product")]
    public class ProductController : Controller
    {
        private readonly IProductService productService;
        private readonly ILogger<ProductController> logger;

        public ProductController(IProductService productService, ILogger<ProductController> logger)
        {
            this.productService = productService;
            this.logger = logger;
        }

        /// <summary>
        /// Get a Product using its unique code
        /// </summary>
        [HttpGet("{code}")]
        [ProducesResponseType(typeof(ProductDto), 200)]
        public async Task<IActionResult> GetAsync(string code)
        {
            try
            {
                logger.LogDebug("Request for Product with {code}", code);
                var product = await productService.GetProductByCodeAsync(code);
                if (product == null) return NotFound();

                var productDto = ProductMapper.MapToDto(product);

                return Json(productDto);
            }
            catch(Exception ex)
            {
                logger.LogError(ex, "Could not get Product with {code}", code);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Get Product summary using the products code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("summary/{code}")]
        [ProducesResponseType(typeof(ProductSummaryDto), 200)]
        public async Task<IActionResult> GetSummaryAsync(string code)
        {
            try
            {
                logger.LogDebug("Request for Product summary with {code}", code);
                var product = await productService.GetProductByCodeAsync(code);
                if (product == null) return NotFound();

                var productDto = ProductMapper.MapTosummaryDto(product);

                return Json(productDto);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Could not get Product summary with {code}", code);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Create a new product
        /// </summary>
        /// <param name="productDto"></param>
        [ProducesResponseType(200)]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ProductDto productDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    logger.LogDebug("Request to create Product with {code}, {product}", productDto.Code, productDto);

                    // map the dto to the product
                    var product = ProductMapper.Map(productDto);
                    await productService.AddProduct(product);
                    return Ok();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Could not create Product with {code}", productDto.Code);
                    return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            else return BadRequest(ModelState);
        }
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]ProductDto productDto)
        {
            var product = ProductMapper.Map(productDto);
            await productService.UpdateAsync(product);
            return Ok();
        }

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            var product = await productService.GetProductByCodeAsync(code);
            if (product == null) return NoContent(); // there is nothing to delete

            await productService.DeleteAsync(product);
            return Ok();
        }

        [HttpGet("Available")]
        public async Task<IActionResult> GetAsync([FromQuery]int pageNo, [FromQuery]int pageSize)
        {
            var pagedProducts = await productService.GetAvailableProductsAsync(pageNo, pageSize);
            var pagedDtos = ProductMapper.MapToPagedDtos(pagedProducts);
            return Json(pagedDtos);
        }

        [HttpGet("filter")]
        public async Task<IActionResult> FilterAsync([FromQuery]int pageNo, [FromQuery]int pageSize, string typeCode, string unitCode, string categoryCode)
        {
            var pagedProducts = await productService.GetProductsFilteredAsync(pageNo, pageSize, typeCode, unitCode, categoryCode);
            var pagedDtos = ProductMapper.MapToPagedDtos(pagedProducts);
            return Json(pagedDtos);
        }
    }
}