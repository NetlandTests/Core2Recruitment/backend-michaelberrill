﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.WebApi.Models;
using System.Threading.Tasks;
using SIENN.WebApi.Mappers;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Category")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetAsync(string code)
        {
            var category = await categoryService.GetCategoryByCodeAsync(code);
            if (category == null) return NotFound();

            var categoryDto = CategoryMapper.MapToDto(category);
            return Json(categoryDto);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CategoryDto categoryDto)
        {
            var category = CategoryMapper.Map(categoryDto);
            await categoryService.AddCategory(category);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CategoryDto categoryDto)
        {
            var category = CategoryMapper.Map(categoryDto);
            await categoryService.UpdateCategory(category);
            return Ok();
        }

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            var category = await categoryService.GetCategoryByCodeAsync(code);
            if (category == null) return NoContent(); // there is nothing to delete

            await categoryService.DeleteAsync(category);
            return Ok();
        }
    }

}