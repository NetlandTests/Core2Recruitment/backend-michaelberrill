﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.WebApi.Models;
using System.Threading.Tasks;
using SIENN.WebApi.Mappers;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Unit")]
    public class UnitController : Controller
    {
        private readonly IUnitService unitService;

        public UnitController(IUnitService unitService)
        {
            this.unitService = unitService;
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetAsync(string code)
        {
            var unit = await unitService.GetAsync(code);
            if (unit == null) return NotFound();

            var unitDto = UnitMapper.MapToDto(unit);
            return Json(unitDto);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UnitDto unitDto)
        {
            var unit = UnitMapper.Map(unitDto);
            await unitService.Add(unit);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]UnitDto typeDto)
        {
            var unit = UnitMapper.Map(typeDto);
            await unitService.Update(unit);
            return Ok();
        }

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            var unit = await unitService.GetAsync(code);
            if (unit == null) return NoContent(); // there is nothing to delete

            await unitService.DeleteAsync(unit);
            return Ok();
        }
    }

}