﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.WebApi.Models;
using System.Threading.Tasks;
using SIENN.WebApi.Mappers;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Type")]
    public class TypeController : Controller
    {
        private readonly ITypeService typeService;

        public TypeController(ITypeService typeService)
        {
            this.typeService = typeService;
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetAsync(string code)
        {
            var type = await typeService.GetAsync(code);
            if (type == null) return NotFound();

            var typeDto = TypeMapper.MapToDto(type);
            return Json(typeDto);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TypeDto typeDto)
        {
            var type = TypeMapper.Map(typeDto);
            await typeService.Add(type);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]TypeDto typeDto)
        {
            var type = TypeMapper.Map(typeDto);
            await typeService.Update(type);
            return Ok();
        }

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            var type = await typeService.GetAsync(code);
            if (type == null) return NoContent(); // there is nothing to delete

            await typeService.DeleteAsync(type);
            return Ok();
        }
    }

}