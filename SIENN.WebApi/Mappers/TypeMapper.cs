﻿using SIENN.WebApi.Models;

namespace SIENN.WebApi.Mappers
{
    public class TypeMapper
    {
        internal static DbAccess.Model.Type Map(TypeDto type)
        {
            return new DbAccess.Model.Type
            {
                Code = type.Code,
                Description = type.Description
            };
        }

        internal static TypeDto MapToDto(DbAccess.Model.Type type)
        {
            return new TypeDto
            {
                Code = type.Code,
                Description = type.Description
            };
        }
    }
}
