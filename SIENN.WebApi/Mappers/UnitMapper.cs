﻿using SIENN.WebApi.Models;

namespace SIENN.WebApi.Mappers
{
    public class UnitMapper
    {
        internal static DbAccess.Model.Unit Map(UnitDto unit)
        {
            return new DbAccess.Model.Unit
            {
                Code = unit.Code,
                Description = unit.Description
            };
        }

        internal static UnitDto MapToDto(DbAccess.Model.Unit unit)
        {
            return new UnitDto
            {
                Code = unit.Code,
                Description = unit.Description
            };
        }
    }

}
