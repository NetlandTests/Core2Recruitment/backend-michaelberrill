﻿using SIENN.DbAccess.Model;
using SIENN.WebApi.Models;
using System;
using System.Linq;

namespace SIENN.WebApi.Mappers
{
    public class ProductMapper
    {
        internal static Product Map(ProductDto productDto)
        {
            return new Product
            {
                Code = productDto.Code,
                Description = productDto.Description,
                Price = productDto.Price,
                DeliveryDateUtc = productDto.DeliveryDateUtc,
                IsAvailable = productDto.IsAvailable,
                TypeCode = productDto.TypeCode,
                UnitCode = productDto.UnitCode,
                ProductCategories = productDto.ProductCategoryCodes?.Select(x => new ProductCategory
                {
                    CategoryCode = x,
                    ProductCode = productDto.Code
                }).ToArray()
            };
        }

        internal static ProductDto MapToDto(Product product)
        {
            return new ProductDto
            {
                Code = product.Code,
                Description = product.Description,
                DeliveryDateUtc = product.DeliveryDateUtc,
                IsAvailable = product.IsAvailable,
                Price = product.Price,
                TypeCode = product.TypeCode,
                UnitCode = product.UnitCode,
                ProductCategoryCodes = product.ProductCategories?.Select(x=> x.CategoryCode).ToArray()
            };
        }

        internal static ProductSummaryDto MapTosummaryDto(Product product)
        {
            return new ProductSummaryDto
            {
                ProductDescription = $"({product.Code}) {product.Description}",
                Price = $"{product.Price:N} zł",
                DeliveryDate = product.DeliveryDateUtc.ToString("dd.MM.yyyy"),
                IsAvailable = product.IsAvailable ? "Dostępny" : "Niedostępny",
                Type = $"({product.TypeCode}) {product.Type.Description}",
                Unit = $"({product.UnitCode}) {product.Unit.Description}",
                CategoriesCount = product.ProductCategories?.Count() ?? 0
            };
        }
        internal static PageResult<ProductDto> MapToPagedDtos(PageResult<Product> pagedProducts)
        {
            return new PageResult<ProductDto>()
            {
                ItemsCount = pagedProducts.ItemsCount,
                Page = pagedProducts.Page,
                PageSize = pagedProducts.PageSize,
                Items = pagedProducts.Items.Select(x=> MapToDto(x)).ToArray()
            };
        }
    }
}
