﻿using System;
using System.Threading.Tasks;
using SIENN.DbAccess.Model;
using SIENN.WebApi.Models;

namespace SIENN.WebApi.Mappers
{
    public class CategoryMapper
    {
        internal static Category Map(CategoryDto categoryDto)
        {
            return new Category
            {
                Code = categoryDto.Code,
                Description = categoryDto.Description
            };
        }

        internal static CategoryDto MapToDto(Category category)
        {
            return new CategoryDto
            {
                Code = category.Code,
                Description = category.Description
            };
        }
    }
}
